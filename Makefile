
CC =  gcc
CFLAG =  -O0 -Wall -std=gnu99 -pthread -fms-extensions -g  -I /usr/local/cuda-6.5/include/ -pg #-Wextra 
#TODO: check out pthread options
NVCC = nvcc
CUDACFLAGS = -lcudart -gencode=arch=compute_35,code=compute_35 
CUDA_HOME  = /usr/local/cuda-6.5/lib64  #/usr/local/cuda/lib64


LINKER = gcc -o 
LFLAG =  -Wall -lpthread -L$(CUDA_HOME) -lcudart  -lcuda  #-lnuma
NVFLAG = -Wallx

binaries = 
SRC =  memory.c launch.c vm.c
TEST_SRC = _test_main.c _test_kernel.cu
SRC += TEST_SRC

testcode = mcl_test.c
testbin = test_vm  test_lex test_lex test_sytbl


RM = rm -f


.PHONY: all
all: $(binaries)

%.o: %.cu
	$(NVCC) $< $(CUDACFLAG) -c -o $@

%.o: %.c
	$(CC) $< $(CFLAG) -c -o $@

.PHONY: debug
debug: CC += -DDEBUG
debug: all



#Runs the unit tests
RUN_TEST = ./
GDB = gdb
.PHONY: test
test:	all
	./_test_main
	./_test_vm

.PHONY: clean
clean:
	$(RM) $(binaries) 
	$(RM) $(testbin)
	$(RM) *~
	$(RM) *.o


.PHONY: test_main
test_main: _test_main.o launch.o 
	$(CC) $(LFLAG)  _test_main.o launch.o -o $@


.PHONY: tarball
tarball: clean
	tar -cvf mcl.tar *

.PHONY: info
info: 
	./info.sh

.PHONY: test_all
test_all: $(testbin)	

.PHONY: test_vm
test_vm:
	$(CC) -DTEST_VM  vm.c $(CFLAG) $(LFLAG) -o $@
	$(RUN_TEST)test_vm
.PHONY: debug_vm
debug_vm:
	$(CC) -DTEST_VM  vm.c $(CFLAG) $(LFLAG) -o $@
	gdb ./test_vm


.PHONY: test_lex
test_lex:
	$(CC) -DTEST_LEXER lexer.c $(CFLAG) -o test_lex
	./test_lex
.PHONY: test_parser
test_parser: 
	$(CC) -DTEST_PARSER parser.c $(CFLAG) -o test_parser
	./test_parser
.PHONY: test_sytbl
test_sytbl: 
	$(CC) -DTEST_SYMTBL symtable.c $(CFLAG) -o $@
	./$@
.PHONY: test_runtime
test_runtime:
	$(CC) -DTEST_RUNTIME runtime.c $(CFLAG) $(LFLAG) -o test_parser
	./test_parser

