/**
* @file _test_main.c
* @brief test code
* @author fhtuft
* @bug No Known bugs
*/
#include <stdio.h>

#include "unit_test.h"
#include "launch.h"

int main(void) {
    //fprintf(stdout,"Hello cuda!\n");

    __test_launch("_test_kernel.ptx","test_kernel");

    return 0;
}
