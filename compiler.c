/* compiler for mcl-lang  
get called by user, calls parser and code gen. keepings everyone happy!
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "parser.h"

#define MCL_OK   0
#define RETURN_ERROR(ERR)  do{if((ERR) < MCL_OK) {err = (ERR);goto ERROR_RETURN;}}while(0)


/*the common  struct, put this in own header */
typedef struct mcl_common_{
    uint64_t *stack;
    size_t glob_sp;
    size_t stack_len;
}mcl_common_s;




int compile(char *inputstr,mcl_common_s *com) {
    assert(cmdstr);
    
    /* The parsetree generated from input string */
    parse_tree_s = *root; //linear tree..

    RETURN_ERROR(parser(inputstr,&root));
    //RETURN_ERROR(sem_anlys);    
    //RETURN_ERROR(cod_gen());
        

    return 0;
    ERROR_RETURN:
    
    return -1;

}




