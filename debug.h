/*
    Debug
*/
#ifndef DEBUG_H
#define DEBUG_H

#ifndef DEBUG 
#define debug_print(M, ...)
#else

#include <stdio.h>
#define debug_print(M, ...)  fprintf(stderr,"DEBUG: %s:%s:%d:" M "\n",__FILE__,__func__,__LINE__,##__VA_ARGS__)

#endif 



#endif

