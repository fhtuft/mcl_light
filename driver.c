#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <cuda.h>
#include <builtin_types.h>

#define CU_DRIVER_INIT_FLAG 0

#define DRIVER_ERROR(ERR) do{if((ERR) != CUDA_SUCCESS){const char *err_string; cuGetErrorString((ERR),&err_string); fprintf(stderr,"MCL_CUDA_ERROR in %s %d: %s \n",__FUNCTION__,__LINE__,err_string); goto ERROR_DRIVER; }}while(0)


typedef struct driver_ {
    int device_count;
    CUcontext *contexts;
     
    

}driver_s;


int _init_driver(driver_s *ptr) {

    DRIVER_ERROR(cuInit(CU_DRIVER_INIT_FLAG));
    
    return 0;
    ERROR_DRIVER:
    return -1;
}



