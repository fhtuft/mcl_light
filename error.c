/* 
 *Error handling for mcl 
*/
#define  MAX_ERROR_LENGTH = 50



struct mcle {
  int error;
  char msg[MAX_ERROR_LENGTH];
};


const char * get_error_str(const int error) {
  
    static const struct mcle et[] = {
        {MCL_OK, "MCL OK"},
        {MCL_ERROR, "MCL ERROR"},
        {MCL_NOTATYPE,"MCL NOT A NOWN TYPE"},
        { MCL_NOTIMPLEMENTED, "MCL NOT IMPLEMENTED" }
        /*TODO: Add more errors her */
    };
    return er[error].msg; 
}
