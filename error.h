/*
 * Error handling for mcl
*/
#ifndef MCL_ERROR_H
#define MCL_ERROR_H
 


#include <stdio.h> //perror,fprintf
#include <stdlib.h>//exit,
#include <cuda.h>
#include <builtin_types.h>


//#define BREAK_ON_ERROR(ERROR) if((ERROR) < MCL_OK){error = ERROR;break;}
//#define EXIT_ERROR(MSG) do{fprintf(stderr,"EXIT FAILURE,%s FILE:%s LINE:%d \n",MSG,__FILE__,__LINE__); exit(EXIT_FAILURE);}while(0)
//#define MALLOC_ERROR(PTR) do {if((PTR) == NULL) {perror("Malloc"); EXIT_ERROR("");} }while(0)
//#define GOTO_ERROR_RETURN(ERROR_CODE) do{error =(ERROR_CODE); goto ERROR_RETURN; }while(0)


static CUresult inline _driver_error(CUresult err,const char* ffname,const int line_nr) {
    if(err != CUDA_SUCCESS) {
        const char *err_string; 
        cuGetErrorString((err),&err_string); 
        fprintf(stderr,"MCL_CUDA_ERROR in %s %d: %s \n",ffname,line_nr,err_string); 
    }
    return err;
}

#define DRIVER_ERROR(ERR) if(_driver_error((ERR),__FUNCTION__,__LINE__) != CUDA_SUCCESS){goto ERROR_DRIVER;} 

#define MCL_ERROR(ERR) (err = (ERR); if(err < MCL_OK){goto ERROR_MCL;}  )
#define RETURN_ERROR(ERR)  if((ERR) < MCL_OK){goto ERROR;}


/* CONSTS */

enum {
  /* OK */
  MCL_OK = 0,
  /* GENERAL ERROR */
  MCL_ERROR = -1,
  /* NOT IMPLEMENTED */
  MCL_NOTIMPLEMENTED = -2,
  /* NOT A BASIC TYPE */
  MCL_NOTATYPE = -3,
  /* FULL TASKLIST */
  MCL_TASKLISTFULL = -4,
  /* TASK NOT FOUND */
  MCL_TASKNOTFOUND  = -5,
  /* NOT A TASK */
  MCL_NOTATASK     = -6,
  /* OUT OF BOUND */
  MCL_OUTB       = -7,
  /* List is full*/
  MCL_LISTFULL   = -8,
  /* CUDA ERROR*/
  MCL_CUDAERROR   = -9,
  /* ELEMENT NOT FOUND LIST*/
  MCL_ELEMENTNFOUND_LIST  = -10,
  /* Zero param return from param constructor */
  MCL_ZEROPARAM    = -11,
  /* NO NEIGBHOR AT THAT LOCKATION */
  MCL_TOPONONEIGHBOR = -12,
  /* This is a return valueto keep the compiler happy, should never be returned*/
  MCL_HAPPYCOMPILER = -13,
  /* No cuda devices found */
  MCL_NOCUDADEVICES = -14,
  /* DUMMY FUNC ENCONTERD */
  MCL_DUMMYFUNC = -15,
  /* NUMBER OF ERRORS, THIS IS NOT A VALID ERROR CODE */
  MCL_NUMOFERRORS   = -16
};






#endif
