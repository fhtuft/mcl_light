/**
* @file init.c
* @brief inits mcl light
* @author fhtuft
* @bug No Known bugs
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>

#include <cuda.h>
#include <builtin_types.h>


#define INIT_CUDA_DRIVER_API 0
typedef int mcl_error;


typedef struct devices_ {
    int deviceCount;
    CUdevice *devices;
    CUcontext *contexts;
}devices_s;

mcl_error __init_devices(devices_s *const dev) {
    
    DRIVER_ERROR(cuInit(INIT_CUDA_DRIVER_API));
    

    return MCL_OK;

ERROR_DRIVER:
    return MCL_DRIVER_API_ERROR;
}
