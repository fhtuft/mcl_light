/**
* @file launch.c
* @brief launching kernels for mcl light
* @author fhtuft
* @bug No Known bugs
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <string.h>
#include <cuda.h>
#include <builtin_types.h>


typedef struct {
   CUcontext ctx; 
}devices_s;
#define MAX_GPU 8

devices_s devices[MAX_GPU];

#define CU_DRIVER_INIT_FLAG 0

#define DRIVER_ERROR(ERR) do{if((ERR) != CUDA_SUCCESS){const char *err_string; cuGetErrorString((ERR),&err_string); fprintf(stderr,"MCL_CUDA_ERROR in %s %d: %s \n",__FUNCTION__,__LINE__,err_string); goto ERROR_DRIVER; }}while(0)

int _init_driver(void) {
    DRIVER_ERROR(cuInit(CU_DRIVER_INIT_FLAG));
    
    return 0;
    ERROR_DRIVER:
    return -1;
}

void __test_launch(const char *moduleName,const char *kernelName) {
        DRIVER_ERROR(cuInit(0));
        int deviceCount;
        DRIVER_ERROR(cuDeviceGetCount(&deviceCount)); 
        printf("device count %d \n",deviceCount); 
         
        //deviceCount = 1;    
            
	void *k_params[3];
	memset(k_params,0,sizeof(k_params));
	
	CUdeviceptr c[8];

	int a,b;
	k_params[0] = (void*)&a;
	k_params[1] = (void*)&b;
	//k_params[2] = (void*)&c;

	a = 1; b = 2;
	
       fprintf(stderr,"create contex!\n");
       for(int i = 0; i< deviceCount;i++) {
            CUdevice dev;
            DRIVER_ERROR(cuDeviceGet(&dev,i));
            devices_s *device = &devices[i];
            DRIVER_ERROR(cuCtxCreate(&(device->ctx), 0, dev));
            DRIVER_ERROR(cuCtxPopCurrent(&(device->ctx)));        
        }
       fprintf(stderr,"cuAlloc! \n");
       for(int i = 0; i < deviceCount; i++) {
	 devices_s *device = &(devices[i]);
	 DRIVER_ERROR(cuCtxPushCurrent(device->ctx)); 
	 DRIVER_ERROR(cuMemAlloc(&c[i],sizeof(int)));
	 DRIVER_ERROR(cuCtxPopCurrent(&(device->ctx)));
       }
       fprintf(stderr,"load module! \n");
       for(int i = 0; i< deviceCount; i++) {
	 
	        k_params[2] = &c[i];
	        devices_s *device = &(devices[i]);

	        DRIVER_ERROR(cuCtxPushCurrent(device->ctx));
	        CUmodule module;
	        printf("moduleName: %s\n",moduleName);
	        DRIVER_ERROR(cuModuleLoad(&module,moduleName));
	        CUfunction function;
	        printf("kernelName: %s\n",kernelName);
	        DRIVER_ERROR(cuModuleGetFunction(&function, module, kernelName));
	        DRIVER_ERROR(cuLaunchKernel(function,1,1,1,1,1,1,0,0,k_params,NULL));
	        DRIVER_ERROR(cuCtxPopCurrent(&(device->ctx)));

        }

        for(int i =0; i<deviceCount; i++) {
	        int ch;
            devices_s *device = &devices[i];
            DRIVER_ERROR(cuCtxPushCurrent(device->ctx));        
	        DRIVER_ERROR(cuMemcpyDtoH(&ch,c[i],sizeof(int)));
	        fprintf(stdout,"c: %d \n",ch);
            DRIVER_ERROR(cuCtxDetach((device->ctx)));
            //DRIVER_ERROR(cuCtxPopCurrent(&(device->ctx)));
        }   

       
 
      return;  
ERROR_DRIVER: //jump point for DRIVER_ERROR
    fprintf(stderr,"Driver error! \n");  
    
} 

