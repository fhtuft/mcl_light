#ifndef LAUNCH_H
#define LAUNCH_H
/**
* @file launch.h
* @brief launching kernels for mcl light
* @author fhtuft
* @bug No Known bugs
*/

void __test_launch(const char *moduleName,const char *kernelName);



#endif 
