/* 
    This is a direct coded lexer for mcl-lang 
*/
///#define TEST_LEXER

#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>

#include "lexer.h"


/* Builtins */ 
static const char all[]    =  {"all "}; 
static const char launch[] =  {"launch "};//  ' ' is for length of builtin 
static const char load[]   =  {"load "};
static const char on[]     =  {"on "};  
static const char recv[]   =  {"recv "};
static const char send[]   =  {"send "};
static const char uload[]  =  {"uload "};
#ifdef TEST_LEXER
//This must be updatet manuly, with is travesty
static const size_t T_builtin_count = 7;
static const char T_all_builtins[] = {"all launch load on recv send uload "};
#endif

#define BUILTIN_OFFSET  3
#define BUILTIN_WHITE   ' '


static int inline isseperator(const char c) {
    return c == ':';
}
static int inline isterminator(const char c) {
    return c == '\0';
}


int get_next_token(lexstr_s * ls ,token_s * token) {
    //assert(s != NULL);
    //assert(token  != NULL);

    int index= 0;           
    char c,*string = ls->start;       
    //assert(string != NULL);
   
 
    START:
        c = string[index];
        if(isblank(c)) {string++; goto START;}  
        
        if(isalpha(c))      {index; goto BUILTIN;} //To keep things clean.  
        if(isdigit(c))        {++index; goto NUM;}        
        if(isseperator(c))  {++index; goto SEPARATOR;}
        if(c == '\0')       {goto END_OF_STRING;}  
        goto LEXER_ERROR;
    BUILTIN:
        index++;
        switch(c) {
            case 'a':
                goto ALL;        
            case 'l':
                goto LAUNCH;
          //case 'l': LAUNCH handels LOAD
          //    goto LOAD; 
            case 'o':
                goto ON;
            case 'r':
                goto RECV;
            case 's':
                goto SEND;
            case 'u':
                goto ULOAD;
            default:    
                goto IDENT;
        };

    ALL:
        c = string[index];
        if(isalpha(c)) {
            if(c == all[index])               {index++; goto ALL;}
            else                              {index++; goto IDENT;}
        }
        if(isdigit(c))                      {index++; goto NUM;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == all[index])   {token->type = ALL_T; goto ACCEPT;}
            else                              {token->type = IDENT_T; goto ACCEPT;}
        }
        goto LEXER_ERROR;
                  
    LAUNCH:
        c = string[index];
        if(isalpha(c)) {
            if(c == launch[index])             {index++; goto LAUNCH;}
            else if(c == load[index])          {index++; goto LOAD;}
            else                               {index++; goto IDENT;}
        }
        if(isdigit(c))                         {index++; goto NUM;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == launch[index]) {token->type = LAUNCH_T; goto ACCEPT;}
            else                               {token->type = IDENT_T;  goto ACCEPT;}
        }
        goto LEXER_ERROR;

    ON:
        c = string[index];
        if(isalpha(c)) {
            if(c == on[index])                {index++; goto ON;}
            else                              {index++; goto IDENT;}
        }
        if(isdigit(c))                      {index++; goto NUM;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == on[index])    {token->type = ON_T; goto ACCEPT;}
            else                              {token->type = IDENT_T; goto ACCEPT;}
        }
        goto LEXER_ERROR;

    RECV:
        c = string[index];
        if(isalpha(c)) {
            if(c == recv[index])             {index++; goto RECV;}
            else                             {index++; goto IDENT;}
        } 
        if(isdigit(c))                      {index++; goto IDENT;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == recv[index]) {token->type = RECV_T;  goto ACCEPT;}
            else                             {token->type = IDENT_T; goto ACCEPT;}
        }
        goto LEXER_ERROR;
   
    SEND:
        c = string[index];
        if(isalpha(c)) {
            if(c == send[index])             {index++; goto SEND;}
            else                             {index++; goto IDENT;}
        } 
        if(isdigit(c))                      {index++; goto IDENT;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == send[index]) {token->type = SEND_T;  goto ACCEPT;}
            else                             {token->type = IDENT_T; goto ACCEPT;}
        }
        goto LEXER_ERROR;
     
    LOAD:
        c = string[index]; 
        if(isalpha(c)) {
            if(c == load[index])             {index++; goto LOAD;}
            else                             {index++; goto IDENT;}
        }
        else if(isdigit(c))                  {index++; goto IDENT;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == load[index]) {token->type = LOAD_T;  goto ACCEPT;}
            else                             {token->type = IDENT_T; goto ACCEPT;}
        }
        goto LEXER_ERROR;
    
    ULOAD:
        c = string[index]; 
        if(isalpha(c)) {
            if(c == uload[index])            {index++; goto ULOAD;}
            else                             {index++; goto IDENT;}
        }
        else if(isdigit(c))                   {index++; goto IDENT;}
        if(isblank(c)) {
            if(BUILTIN_WHITE == uload[index]) {token->type = ULOAD_T; goto ACCEPT;}
            else                              {token->type = IDENT_T; goto ACCEPT;}
        }
        goto LEXER_ERROR;

      
    IDENT:
        c = string[index];
        if(isalpha(c))      {++index; goto IDENT;}        
        if(isdigit(c))        {++index; goto IDENT;}        
        if(isblank(c)) {token->type = IDENT_T; goto ACCEPT;}
        goto LEXER_ERROR;

    NUM:
        c = string[index];
        if(isdigit(c))        {++index; goto NUM;}        
        if(isblank(c)) {token->type = NUMBER_T; goto ACCEPT; }
        goto LEXER_ERROR;
        
    SEPARATOR:
        c = string[index];
        if(isblank(c)) {token->type = SEPARATOR_T; goto ACCEPT;}
        if(isterminator(c)) {token->type = SEPARATOR_T; goto ACCEPT;}  
        goto LEXER_ERROR;      

   
    ACCEPT:
        switch(token->type)  {
            case IDENT_T:
                if(index > MAX_LENGTH_IDENT) {goto LEXER_ERROR;}
                memcpy(token->string,string,index);
                memset(token->string + index,'\0',MAX_LEN_TOKEN + 1 - index); 
                break;
            case NUMBER_T:
                if(index > MAX_LENGTH_NUM)   {goto LEXER_ERROR;}
                memcpy(token->string,string,index);
                memset(token->string + index,'\0',MAX_LEN_TOKEN + 1 - index); 
                token->num = atoi(token->string);
                break;    
            default:
                break;
        } 
        /* Update lexer string */       
        ls->end = &string[index];
        ls->state = IN_STRING_S;
        
        return 0;
    END_OF_STRING:
        token->type = NOT_A_TOKEN_T;
        ls->end = &string[index];
        ls->state = EOS_S;
        
        return 0;        
    LEXER_ERROR:
        token->type = NOT_A_TOKEN_T;
        ls->end = &string[index];
        ls->state = LEXER_ERROR_S;
        //TODO: cpy in string for err handling i parser ?
        return -1; //TODO: more error msg?
}

#ifdef TEST_LEXER
#include <string.h>
#include "unit_test.h"
#define INIT_LEX_STR(LS,STRING) (LS)->string = (STRING); (LS)->start = (STRING);(LS)->end = NULL
#define NEXT_LEX_STR(LS)  (LS)->start = (LS)->end; (LS)->end = NULL

int main(int argc,char *argv[])  {

    START_TEST;

    token_s t;
    char string1[] = {"launch   load test1 11 :"};
    char err_str1[] = {"1test"};
    char err_str2[] = {"aaaaaaaaa"};//MAX_LENGHT_IDENT
    _Static_assert(MAX_LENGTH_IDENT < sizeof err_str2 - 1,"err_str2 to short!");

    lexstr_s ls;
    INIT_LEX_STR(&ls,string1);  
 
    PRINT_TEST("START test of lexering of basic string: string1 ");

    ASSERT_TEST("get_next_token",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("token type LAUNCH_T",LAUNCH_T == t.type);
    NEXT_LEX_STR(&ls);

    ASSERT_TEST("get_next_toke",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("token type LOAD_T",LOAD_T == t.type);
    NEXT_LEX_STR(&ls); 

    
    ASSERT_TEST("get_next_toke",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("token type IDENT_T",IDENT_T == t.type);
    ASSERT_TEST("is string: test1",strcmp(t.string,"test1") == 0);
    NEXT_LEX_STR(&ls); 

 
    ASSERT_TEST("get_next_toke",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("token type NUMBER_T",NUMBER_T == t.type);
    ASSERT_TEST("is  11",t.num == 11);
    NEXT_LEX_STR(&ls); 

     
    ASSERT_TEST("get_next_toke",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("token type SEPERATOR_T",SEPARATOR_T == t.type);
    NEXT_LEX_STR(&ls);

 
    ASSERT_TEST("get_next_toke",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("state EOS_S",EOS_S == ls.state);
    ASSERT_TEST("token type NOT_A_TOKEN",NOT_A_TOKEN_T == t.type);    
    NEXT_LEX_STR(&ls); 

    PRINT_TEST("LOOP EOS!");
    ASSERT_TEST("get_next_token",get_next_token(&ls,&t) == 0);
    ASSERT_TEST("state EOS_S",EOS_S == ls.state);
    ASSERT_TEST("token type NOT_A_TOKEN",NOT_A_TOKEN_T == t.type);
    NEXT_LEX_STR(&ls); 

    PRINT_TEST("TEST ERROR STRINGS");

    INIT_LEX_STR(&ls,err_str1);  

    ASSERT_TEST("get_next_token is,string start with num",get_next_token(&ls,&t) !=  0);
    ASSERT_TEST("token type NOT_A_TOKEN_T",NOT_A_TOKEN_T == t.type);

    INIT_LEX_STR(&ls,err_str2);  

    ASSERT_TEST("get_next_token is, to long string",get_next_token(&ls,&t) !=  0);
    ASSERT_TEST("token type NOT_A_TOKEN_T",NOT_A_TOKEN_T == t.type);

 
    PRINT_TEST("TEST OF ALL BUILTINS");
    
    INIT_LEX_STR(&ls,(char *)T_all_builtins); //cast from const for happy compiler
    
    for(int i = START_BUILTIN_TEST +1; i <END_BUILTIN_TEST; i++ ) {
        ASSERT_TEST("get_next_toke",get_next_token(&ls,&t) == 0);
        ASSERT_TEST("TESTING BUILTINS..",i == t.type);
        NEXT_LEX_STR(&ls); 

    }

 
    END_TEST;

}
#endif

