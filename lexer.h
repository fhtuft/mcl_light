/*
    lexer h
*/
#ifndef LEXER_H
#define LEXER_H

#define MAX_LEN_TOKEN      8 //lexem.. 
 
#define MAX_LENGTH_NUM     8 //this is short
#define MAX_LENGTH_IDENT   8


typedef enum token_e {
    NOT_A_TOKEN_E = -1,
    IDENT_T = 0,
    NUMBER_T,
    SEPARATOR_T,
    /*START BUILTINS*/
    START_BUILTIN_TEST,//used for unit testing builtins, NOT A VALID TOKEN
    ALL_T,
    LAUNCH_T,
    LOAD_T,
    ON_T,
    RECV_T,   
    SEND_T,
    ULOAD_T,
    VAR_T,
    END_BUILTIN_TEST,//used for unit testing builtins, NOT A VALID TOKEN
    /*END BUILTINS*/
    NOT_A_TOKEN_T,
    TOKEN_COUNT
}token_e;

const char *token_e_to_str[] = {
    "IDENT",
    "NUMBER",
    "SEPARATOR",
    "START_BUILTIN_TEST",
    "ALL",
    "LAUNCH",
    "LOAD",
    "ON",
    "RECV",   
    "SEND",
    "ULOAD",
    "VAR",
    "END_BUILTIN_TEST",
    "NOT_A_TOKEN",
}; 


typedef enum lexer_state_ {    
    IN_STRING_S = 0,
    EOS_S,
    LEXER_ERROR_S
}lexstate_e;

typedef struct token_ {
    token_e type;
    int num;       //value of NUM
    char string[MAX_LEN_TOKEN+1];
    
    struct token_ *next;
    struct token_ *prv;
    
    /* Added under semantic analyses */
    sytbl_element_type_e sym_type;
    union {
        int sym_num;
        void * sym_ptr;
    };

}token_s;

typedef struct lexer_string_ {
    lexstate_e state;
    char *start;
    char *end;
    char *string;//CONST
}lexstr_s;


int get_next_token(lexstr_s * ls ,token_s * token);

static inline void clear_token(token_s *tkn) { 
    memset(tkn,0,sizeof *tkn);
};

static void inline init_lexstr(char *str,lexstr_s *ls) {
    memset(ls,0,sizeof *ls);
    ls->string = str;
    ls->start = ls->string;
}
static void inline updte_lexstr(lexstr_s *ls) {
    ls->start = ls->end;
    ls->end = NULL; //Not sure about this
}



#endif
