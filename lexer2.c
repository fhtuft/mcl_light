/* This is the lexer for mcl-lang */

#include <stdio.h>
#include <string.h>



typedef enum token_e {
    IDENT_T = 0,
    NUMBUR_T,
    LAUNCH_T,
    SEND_T,
    RECV_T,   
    LOAD_T,
    ULOAD_T,
    TOKEN_COUNT
}token_e;



typedef enum state_e {
    ERROR_S = -1,
    REJECT_S = 0, //REJECT STATE OF DFA
    WHITE_S,      //ACEPTING STATE OF DFA
    SEPER_S,      //SEPERATOR
    IDENT_S,
    NUM_S,
    LAUNCH_S,
    SEND_S,
    RECV_S,
    LOAD_S,
    ULOAD_S,
    STATE_COUNT 
}state_e;

#define NUM        128
#define WHITESPACE 129
static const unsigned char alphabet = {'a','b','c','d','e','f','g','h','i','j','k','l','m','o','p','q','r','s','t','u','v','w','x','y','z',
    'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
    NUM,WITHSPACE};
/* a  b  c  d  e  f  g  h  i  j  k  l  m  n  o  p  q  r  s  t  u  v  w  x  y  z  */
/* A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z*/
   /* NUM,WITHSPACE */

static const int transition[sizeof(alphabet)][TOKEN_COUNT] = 
{
              /* a     b     c     d     e     f     g     h     i    j      k     l     m     n     o     p     q       r     s        t      u      v     w     x     y    z*/       
     /*START*/{IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,LAUNCH_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,RECV_S,SEND_S,IDENT_S,ULOAD_S,IDNET_S,SEND_S,IDENT_S,ULOAD_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A    B     C     D     E     F     G     H     I     J     K     L     M     N     O     P    Q     R     S     T     U     V     W     X     Y     Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* NUM,WITHSPACE */
    NUM_S,ERROR_S},       
            /* a      b      c    d     e     f     g      h    i      j    k     l     m     n     o     p     q     r     s     t     u     v     w     x     y    z*/
    /*IDENT*/{IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDNET_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A    B      C    D     E     F     G     H     I     J     K     L     M     N    O      P     Q     R     S    T      U     V     W     X     Y    Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S
    /* NUM,WHITE */
    ,IDENT_S,ERROR_S},
              /* a      b      c      d      e      f      g      h      i      j      k      l       m     n      o      p      q      r      s      t     u      v        w     x    y    z  */
    /*NUMBER*/{REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,
    /* A  B  C  D  E  F  G  H  I  J  K  L  M  N  O  P  Q  R  S  T  U  V  W  X  Y  Z*/
    REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,REJECT,
    /* NUM,WHITESPACE */
    NUM,WHITE},
              /*   a       b       c       d       e       f       g       h       i        j      k        l       m       n       o        p      q       r      s        t       u       v       w       x      y       z  */
    /*LAUNCH*/{LAUNCH_S,IDENT_S,LAUNCH_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,LAUNCH_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,LAUNCH_S,LOAD_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,LAUNCH_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A       B       C      D       E        F       G      H       I       J       K       L       M       N       O       P       Q       R       S      T       U         V      W       X       Y       Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* NUM,WITHSPACE */
    IDENT_S,WITH_S},
             /* a     b       c       d       e       f       g       h       i        j      k      l        m       n       o       p      q       r      s        t        u       v       w       x      y       z  */
    /*LOAD*/{LOAD_S,IDENT_S,IDENT_S,LOAD_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A       B       C      D       E        F       G      H       I       J       K       L       M       N       O       P       Q       R       S      T       U         V      W       X       Y       Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* NUM,WITHSPACE */
    IDENT_S,WITH_S},
 /*SEND*/{IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A       B       C      D       E        F       G      H       I       J       K       L       M       N       O       P       Q       R       S      T       U         V      W       X       Y       Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* NUM,WITHSPACE */
    NUM_S,WITH},
 /*RECV*/{IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A       B       C      D       E        F       G      H       I       J       K       L       M       N       O       P       Q       R       S      T       U         V      W       X       Y       Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* NUM,WITHSPACE */
    NUM_S,WITH},
 /*ULOAD*/{IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* A       B       C      D       E        F       G      H       I       J       K       L       M       N       O       P       Q       R       S      T       U         V      W       X       Y       Z*/
    IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,IDENT_S,
    /* NUM,WITHSPACE */
    NUM_S,WITH},
};


static const unsigned char accepting[TOKEN_COUNT] = {};


static int inline iswhitespace(const char *c) {
    return c <= '\n' && c >= '\b'; 
}
static int inline isnum(const char *c) {
    return c <= '9' && c >= '0';
}
static int inline isupper(const char *c) {
    return c <= 'A' && c >= 'Z';
}
static int inline islower(const char *c) {
    return c <= 'a' && >= 'z';
}
static int inline isalpha(const char *c) {
    return islower(c) || isupper(c);
}



int _get_next_token(char *string, token_s *s) {
        assert(string != NULL);
        assert( string != NULL);
            
        /* remove leading whitespace */
        for(;iswhitespace(*string); string++);  

        /* lexer */
        for(int state = START_S; *string != '\0';string++) {

            state = transition[state][*string];
            



        }
        
}


