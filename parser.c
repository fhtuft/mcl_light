#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>
#include <boolean.h> //for parse_node_s

#include "lexer.h"

#define MCL_OK  0
#define RETURN_ERROR(ERR)  do{if((ERR) < MCL_OK) {err = (ERR);goto ERROR_RETURN;}}while(0)
#define ERROR_PARSE  -3
#define NOP ;
#define TRUE  1
#define FALSE 0

#define PARSE_ERROR(ERR) do{err = ERR; if(_err < MCL_OK){goto ERROR_PARSE;}}while(0)

#define PARSE_MAXCHILD   3

/*the common  struct, put this in own header */
typedef struct mcl_common_{
    uint64_t *stack;
    size_t glob_sp;
    size_t stack_len;
}mcl_common_s;


typedef enum {
    NOT_A_NODE_TYPE_T = -1,
    T_SEND_STMT = 0,
    T_RECV_STMT,
    T_SENDRECV_STMT,
    T_LAUNCH_STMT,
    T_CO_STMT, //COUNCURENT executen
    T_LOOP_STMT,
    T_COMPUND_STMT,
    T_ASSIGN_STMT,
    T_HALT_STMT,
    T_ADD_EXP,
    T_SUB_EXP,
    T_MUL_EXP,
    T_MOD_EXP,
    T_LIT_EXP, //LITERAL
    T_LIT_STR_EXP,
    T_LIT_INT_EXP,
    T_VAR_EXP,
    T_IDENT,
    T_DECLERATION,
    NT_PROGRAM,
    NT_STMT_SEQNS,
    NT_STMT,
    NT_EXP,
    NT_FACTOR,  
    NT_DEC_SEQNS,
    NODE_TYPE_E_COUNT
}node_type_e;


struct _id_node {
    char name[];
}
struct _num_node {
    int num;
};
struct _launch_stmt {
    bool all;
};
typedef struct parse_node_ {

    node_type_e type;

    struct parse_node_ *child; // leftmost child right-sibling
    struct parse_node_ *sibling;
    int child_count;

    union {
        struct _id_node;
        struct _num_node;
        struct _launch_stmt;
    };


}parse_node_s;



typdef struct parse_tree_ {
    
    parse_node  *program;    
    
    /*tmp vars used under building of the parse tree, not to be used after */
    token_s *next_token; 
    parse_node_s *parent; //Node to make ref of current node
    lextstr_s *lexstr;
    
}parse_tree_s;


static inline parse_tree_s* alloc_parse_tree(void) {
    parse_tree_s *tree  = (parse_tree_s*)malloc(sizeof *tree);
    memset(tree,0,sizeof *tree); 
}
static inline free_parse_tree(parse_tree_s *tree) {

}

static parse_node_s* alloc_parse_node(parse_node_e type) {
    parse_node_s *node = (parse_node_s*)malloc(sizeof(parse_node_s));
    memset(node,0,sizeof *node);
    node->type = type;
    return node;
}
static inline void free_parse_node(parse_node_s *node) {
    
    free_childres(node);

    free(node);
} 

#define GET_NEXT_TOKEN do{clear_token(next_token);  get_next_token(lexstr,next_token); updte_lexstr(lexstr); }while(0)
#define MATCH(TKN)  if((TKN) == current_token->type) {GET_NEXT_TOKEN;} else{fprintf(stderr,"Mach Error: Got %s expected %s \n",token_e_to_str[current_token->type], token_e_to_str[(TKN)] ); goto PARSE_ERROR;} 
#define MATCH_ON_TYPE(TOK,TYP)({ token_s *_tok = (TOK);\
    (_tok->type == (TYP)) ? (GET_NEXT_TOKEN,1) : 0; });
#define IS_TYPE(TOK,TYP)  \
({ token_s *_tok = (TOK); \
_tok->type == (TYP); })  

int parse(char *input_str, parse_tree_s **parse_tree) 
{
    int err;
    parse_tree_s *ptree;
    lexstr_s lexstr;
    init_lextr(input_str,&lexstr);
    token_s next_token;
    clear_token(&next_token);
 
    /* Get a parse tree */
    parse_tree_s *ptree = parse_tree_alloc();
    /*set tmp var ref in parse tree */
    ptree->next_token = &next_token;
    ptree->lexstr = &lexstr;    

    /* Build the  parse tree */
    PARSE_ERROR(program(ptree)); 

    /* Remove ref to tmp vars from parse tree */
    ptree->next_token = NULL;
    ptree->lexstr = NULL;
    ptree->parent = NULL;

    /* Return parse tree */
    *parse_tree = ptree;

    return 0;
    
ERROR_PARSE:
    //check err
    return -1;
}



/*Helper function for parse nodes*/
static void add_child(parse_node_s * parent,parse_node_s * node) 
{
    parse_node_s **tmp = &parent->chile;
    //Enq
    while(*tmp) 
        tmp = &(tmp->sibling);
    *tmp = node;
    parent->child_count++;
}
static void free_children(parse_node_s *parent) {
    
    for(;parent->child_count;parent->child_count--) {
        parse_node_s *tmp = parent->child;
        parent->child = parent->child->sibling;
        tmp->sibling = NULL;
        free_parse_node(tmp->siblings);
    }    
}

static parse_node_s* get_child(size_t index, parse_node_s parrent) 
{
    assert(parrent->child_count > index);
    
    parse_node *tmp;
    for(tmp = parrant->child; index; index--)
        tmp = tmp->sibling;
        
    assert(tmp != NULL);
    return t; 
}

/*
    Make a non terminal program node
*/
static int program(parse_tree_s * ptree) 
{
    
    /* Make the program node */
    parse_node_s *prog = parse_node_alloc(NT_PROGRAM);
    /* Uptade the parse tree*/
    ptree->prog = prog;
    ptree->cur = ptree->prog;
    
    
    INIT_NEXT_TOKEN(ptree);

    /* program <- [decSequence] [stmtsequence] */    
    GET_NEXT_TOKEN;
   
    switch(next_token->type) {
    case VAR_T:
    //deq_sequnce()
    break; 
    }

    switch(next_token->type) {       
    case LAUNCH_T:
    case SEND_T:
    case RECV_T:
    statment_seqence(ptree);
    break;
    }
    
     
    switch(lexstr->state) {
    case EOS_S:
        //TODO: More stuff her
    return 0;
    case LEXER_ERROR_S: 
    break; 
    default:
        assert(0); //In string
    }

ERROR_LEX:
    return -1;
ERROR_PARSE:


    
    
    
    return 0; //can't really fail, but return value for compatablity
}
static inline int is_stmt(token_s *token) 
{
    switch(token->type) {
    case LAUNCH_T:
    case SEND_T:
    case RECV_T:
    return TRUE;
    default:
    return FALSE;
    }
}

static inline int is_exp(token_s *token) {

    switch(token->type) {

    }

}

/*
    stement_sequnce <- statment {statment_sequence}
*/
static int stmt_sqnc(token_s *next_token, lexstr_s *lexstr,parse_node_s **node)   
{
    assert(is_stmt(next_token);); 
      
    parse_node_s *this = parse_node_alloc(NT_STMT_SEQNS);

    parse_node_s *child;
    do{
        PARSE_ERROR(stmt(next_token,lexstr,&child));
        add_child(this,child);
        
    }while(is_stmt(next_token));
    
    *node = this;
    return 0; 
ERROR_PARSE:
    free_parse_node(this);
    *node = NULL;
    return -1;
}

/*
    stmt <- LAUNCH_STM | SEND_STMT | RECV_STMT
*/
static int stmt(token_s *next_token,lexstr_s *lexstr,parse_node_s **node) 
{
    assert(is_stmt(next_token););
    
    parse_node_s *child, *this = parse_node_alloc(NT_STMT);
    
    switch(next_toke->type) {
    case LAUNCH_T:
    PARSE_ERROR(launch_stmt(next_token,lexstr,child));
    break;
    case SEND_T:
    //
    break;
    case RECV_T:
    //
    break;
    } 
        
    add_child(this,child);
    *node = this;    
    return 0;
ERROR_PARSE:
    free_parse_node(this);
    *node = NULL;
    return -1;
}

/*
    LAUNCH_STMT <- LAUNCH ID ON (ALL|exp {COMMA exp}) SEPARATOR
*/
static int launch_stmt(token_s *next_token,lexstr_s *lexstr,parse_node_s **node) 
{
    assert(next_token->type == LAUNCH_T);
    
    parse_node_s *child,*this = parse_node_alloc(T_LAUNCH_STMT);

    MATCH(LAUNCH_T);
    
    PARSE_ERROR(var_stmt(next_token,lexstr,&child));
    add_child(this,child);

    MATCH(ON_T);

    if(IS_TYPE(next_token,ALL_T)) {
        this->all = true; 
        MACH(ALL_T);
    }else {
        this->all = false;
        assert(0);//Not implemented
    }    

    MACH(SEPARATOR_T);

    *node = this;    
     return 0;
ERROR_PARSE:
    free_parse_node(this);
    *node = NULL;
    return -1;
}

/*
    SEND_STMT <-  SEND ID TO ID  ON (ALL| exp {COMMA exp}) SIZE exp [DEP ID {COMMA ID}] SEPARATOR
                |  SEND VAR [DEP VAR {COMMA VAR}] SEPARATOR //Magic..
                | SEND ID ON exp SIZE exp OFFSET exp TO ID ON exp OFFSET exp {COMMA ID ON exp SIZE exp OFFSET exp TO ID ON exp OFFSET exp} [DEP ID {COMMA ID}] SEPARATOR
*/
static int send_smt(token_s *next_token,lexstr_s *lexstr,parse_node_s **node) 
{
    assert(IS_TYPE(next_token,SEND_T));
    
    parse_node_s *child,*this = parse_node_alloc(T_SEND_STMT);

    MATCH(SEND_T);
    
    PARSE_ERROR(var_stmt(next_token,lexstr,&child));
    add_child(this,child);
    
    if(IS_TYPE(next_token,ON_T)) {
        do {
            
            MACH(ON_T);
        
            PARSE_ERROR(exp(next_token,lexstr,&child));
            add_child(this,child);
        
            MATCH(SIZE_T);
        
            PARSE_ERROR(expression(next_token,lexstr,&child));
            add_child(this,child);
        
            MATCH(OFFSET_T);

            PARSE_ERROR(expression(next_token,lexstr,&child));
            add_child(this,child);
        

        }while(MATCH_ON_TYPE(next_token,COMMA_T));
    }else if(1) {

        assert(0);
    } else if(1) {
        assert(0);
    }   

    MATCH(SEPAROR_T);

    *node = this;    
     return 0;
ERROR_PARSE:
    free_parse_node(this);
    *node = NULL;
    return -1;
}


static int exp(token_s *next_token,lexstr_s *lexstr,parse_node_s **node) 
{
    assert(is_exp(next_token));
    
    parse_node_s *child,*this = parse_node_alloc(NT_EXP);

    switch(next_token->type) {
    case LITORAL_INT_T:
    PARSE_ERROR(litoral_int_exp(next_token,lexstr,&child));
    
    default:
    assert(0);
    }
    
    add_child(this,child);
    
    
    *node = this;    
    return 0;
ERROR_PARSE:
    free_parse_node(this);
    *node = NULL;
    return -1;
}




static int litoral_int_exp(token_s *next_token,lexstr_s *lexstr,parse_node_s **node) 
{
    assert(next_token->type == LITERAL_INT_T);
   
    parse_node_s *this = parse_node_alloc(T_LITERAL_INT_EXP);
    this->num = next_token->num; 
    
    *node = this;    
    return 0;
ERROR_PARSE:
    free_parse_node(this);
    *node = NULL;
    return -1;
}
