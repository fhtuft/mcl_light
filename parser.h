#ifndef PARSER_H
#define PRRSER_H



typedef enum {
    NOT_A_KIND_K = 0,
    SEND_K,
    LAUNCH_K,
    HALT_K,
    OP_KIND_E_COUNT
}opkind_e;

//is a linear tree..
typedef struct parse_tree_{
    int type; //of tree, only linear 
    opkind_e kind;  
    token_s *tokens;
    char *string_end; //end of the string parsed
    parse_tree_ *next;
    pares_tree_ *prv; //Not in use
}parse_tree_s;



int pars(char *string, parse_tree_s * tree);


#endif
