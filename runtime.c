/* This is the runtime system! */

#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <cuda.h>
#include <builtin_types.h>

#define DEBUG  /* If DEBUG is defines, turn on macros in debug.h */
#include "error.h"
#include "debug.h"
#include "symtable.h"

/*Variables injected into the symtable*/

/*The name of contex in symtable */
static const char CUDA_CARD[]      = "CudaCard"; //CUDA[0123..] /* Base name of cuda contex  */
static const char CUDA_DEVICE[]    = "CudaDevice";//+[0123..]   /* Base name of cuda device  */
static const char CUDA_NUM_CARDS[] = "cu_device_count";         /* Name of number of devices */


struct runtime {
    int deviceCount;    

};

struct runtime *current_runtime; //We can only have one.. 

static struct runtime *alloc_runtime(void) {
    struct runtime *rt = (struct runtime*)malloc(sizeof(struct runtime));
    memset(rt,0,sizeof(struct runtime));
    return rt;
}

static void free_runtime(struct runtime *rt) {
    free(rt);
}

/* Helper functions */
static inline void make_contex_name(char ident[MAX_IDENT_LEN +1],int card_number) {
    memset(ident,(int)'\0',MAX_IDENT_LEN+1);
    int ret = snprintf(ident,MAX_IDENT_LEN+1,"%s%d",CUDA_CARD,card_number);
    assert(ret <= MAX_IDENT_LEN +1); //right??
}



/* Init the runtime system */
int runtime_init(struct runtime **runtime) {
    struct runtime *rt = alloc_runtime();
    
    int err; //For MCL_ERROR

    DRIVER_ERROR(cuInit(0));
    int deviceCount;
    DRIVER_ERROR(cuDeviceGetCount(&deviceCount));
    rt->deviceCount = deviceCount;
    debug_print("CUDA device count: %d",deviceCount);
    err = sytbl_elmnt_int((const int)deviceCount,CUDA_NUM_CARDS);
    RETURN_ERROR(err);   


    //Makes a contex on eatch card
    for(int i  = 0; i< deviceCount; i++) {
        char ident[MAX_IDENT_LEN + 1];
        /* Get device */
        CUdevice dev;
        DRIVER_ERROR(cuDeviceGet(&dev,i));
        /* Make a cu contex */
        CUcontext *ctx = (CUcontext*)malloc(sizeof(CUcontext));
        DRIVER_ERROR(cuCtxCreate(ctx, 0, dev));
        DRIVER_ERROR(cuCtxPopCurrent(ctx));   
        /* Make a ident for created contex*/
        make_contex_name(ident,i);       
        /* Insert contex into symtabl */      
        err = sytbl_elmnt_cucontex(ctx,ident);
        RETURN_ERROR(err);
        debug_print("Made contex on device %d",i);
    }
    
    /* Return runtime */ 
    *runtime = rt;

ERROR_DRIVER:
    *runtime = NULL;
    return -1; //Driver error
ERROR:
    *runtime = NULL;
    return err;
}

int destroy_runtime(struct runtime *rt) {
        
    int err;
    
    /* Get device count */
    int deviceCount;
    err = sytbl_lookup_int(&deviceCount,CUDA_NUM_CARDS); 
    RETURN_ERROR(err);

    /* Destroy all cuda contexs*/
    for(int i = 0; i < deviceCount; i++) {
        /* Build contex name */
        char ident[MAX_IDENT_LEN +1];
        make_contex_name(ident,i);
        /* Get the cuda contex*/
        CUcontext *ctx;
        err = sytbl_lookup_cucontex(ctx,ident);
        RETURN_ERROR(err);
        /* Destroy the cuda contex */
        DRIVER_ERROR(cuCtxDestroy(*ctx)); 
    }
    
    /* Free the runtime struct */
    free_runtime(rt);

ERROR_DRIVER:
    err =  -1;
ERROR:
    return err;


}


#ifdef TEST_RUNTIME
#include "unit_test.h"
#define TEST_RT_CU_NUM_CARDS 2

int sytbl_elmnt_cucontex(CUcontext *ctx,char *ident)                  {return MCL_OK;}
int sytbl_elmnt_int(const int num,const char *ident)                  {return MCL_OK;}

int sytbl_lookup_int(int *num,char *ident) {*num=TEST_RT_CU_NUM_CARDS; return MCL_OK;}
int sytbl_lookup_cucontex(CUcontext *ctx,char *ident)                 {return MCL_OK;}

/* Can only at the pressent do a partial test of func in runtime, as we can not destroy the runtime, we would need acces to the symtabl */
int main(void) {
    struct runtime *rt;
    
    START_TEST;
    runtime_init(&rt);   
    END_TEST;
    return 0;
}

#endif
