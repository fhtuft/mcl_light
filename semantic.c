/* 
This is the semantic analyser for mcl-cc. It only checks that resources are in the symbole table.
*/ 
#include <stdio.h>

#include "symtable.h"
#include "parser.h"
#include "lexer.h"


#define MCL_OK  0
#define RETURN_ERROR(ERR)  do{if((ERR) < MCL_OK) {err = (ERR); goto ERROR_RETURN;
}}while(0)

#define NEXT(PTR) (PTR) = (PTR)->next


/*-- Function prototypes --*/

int semantic(parse_tree_s *root);

static int check_types(const opkind_e kind,token_s *tokens);


/*-- Function implementations  --*/

int semantic(parser_tree_s *root) {
    assert(root != NULL);
    
    int err;
    
    parse_tree_s *tree = root;
    
    do{
        
        const opkind_e kind = tree->kind;
        token_s *ctoken = tree->tokens;             
        
        do {
            const token_e type = ctoken->type;
            
            switch(type) {
                case IDENT_T: 
                    {
                        sytbl_element_s *ste;
                        RETURN_ERROR(!sytbl_lookup(ctoken->ident,&ste));
                        const sytbl_element_type_e sym_type = ste->type;
                        ctoken->sym_type = sym_type;
                        switch(sym_type) {
                            case SYM_INTEGER:
                                ctoken->sym_num = ste->num;
                                break;
                            default:
                                
                                break;
                        }

                    }
                    break;
                default:
                    break;
            }        
            
            //what about halt trees and there tokens,idea insert SEPERATOR_T
            NEXT(ctoken);
        }while(type != SEPERATOR_T);

        NEXT(tree);
    } while(kind != HALT_K)

return MCL_OK;
ERROR_RETURN:
return -1;

}

static int check_types(const opkind_e kind,token_s *tokens) {
    
    
    return -1;

}



#ifdef TEST_SEM


sytbl_element_e *test_elements = (sym_element_e []){{.ident = "Test1",.type = SYM_INTEGER, .num = 7}};

parse_tree_s *test_trees = (parse_tree_s []){{.kind = SEND_K},{.kind = HALT_K}};
token_s *test_tokens = (tokens []){{.type = IDENT_T, .num = 7},{.type = SEPERATOR_T}};

test_trees[0].next = &(test_tree[1].next);
//for 1, next is null from init

test_trees[0].tokens = &test_tokens[0];
test_trees[1].tokens = &test_tokens[1]; //THIS IS NOT IN THE PARSER

test_tokens[0].next = &test_tokens[1];

int test_count = 1;
int cur_test =0;

int s_look_state = 0;
//Test version
int sytbl_lookup(const unsigned char *c, sytb_element_e **e) {
    assert(c != NULL);
    assert(e != NULL);
    
    if(cur_test == 0) {
        if(s_look_state == 0) {
            *e = test_elements[0];
        }else if (s_look_state == 1) {
            *e = test_elements[1];
        }
    }

    s_look_state++;
    return 0;
}


int main(int argc, char *argv[]) {





}

#endif


