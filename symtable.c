/* Symbole table for mcl */
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <assert.h>

#include "symtable.h"


//#define TEST_SYMTBL

/* CONFIG */
#define LAUDEN  //hash algo used
//#define DJB2

#define SYMTBL_LEN                      2003U //prime
#define UPERBOUDN_ASCII                  128U
#define ELEMENT_IDENT_LEN  (MAX_IDENT_LEN +1)

#define MCL_OK
#define RETURN_ERROR(ERR)  if((ERR) < MCL_OK){goto ERROR_RETURN;}


/*-- Globals --*/

static sytbl_element_s *symbol_tbl[SYMTBL_LEN];
#define SYTBL_GET_ELEMENT_PTR(KEY)  (&symbol_tbl[(KEY)])


/*-- Function prototypes  --*/

static sytbl_element_s * sytbl_alloc(void);
static void sytbl_free(sytbl_element_s *ste);
static void sytbl_enforce_ident(const char *,sytbl_element_s *);
//int sytbl_elmnt_int(const int num,const char *ident);

#ifdef DJB2
static uint64_t __djb2(const unsigned char[MAX_IDENT_LEN +1]);
#define HASH(STR) __djb2((STR)) 
#endif
#ifdef LAUDEN
static uint64_t __lauden(const unsigned char[MAX_IDENT_LEN + 1]);
#define HASH(STR) __lauden((STR))
#endif


/*-- Function implementations --*/

void sytbl_init(void) {
    memset(symbol_tbl,0,sizeof symbol_tbl); //NULL
}

void sytbl_destroy(void) {
    fprintf(stderr,"NIP\n");
}

/* INT */
int sytbl_elmnt_int(int num,const char *ident) {
   
    int err;

    sytbl_element_s *elm = sytbl_alloc();
    sytbl_enforce_ident(ident,elm);
    elm->type = SYM_INTEGER;   
    elm->num = num;        
    err = sytbl_insert(elm);
    RETURN_ERROR(err);    

    return 0;
ERROR_RETURN:
    sytbl_free(elm);
    return err;
} 

/* CUdevice */
int sytbl_elmnt_cudevice(CUdevice dev,const char *ident) {
   
    int err;

    sytbl_element_s *elm = sytbl_alloc();
    sytbl_enforce_ident(ident,elm);
    elm->type = SYM_CUDEVICE;   
    elm->dev = dev;        
    err = sytbl_insert(elm);
    RETURN_ERROR(err);
   
    return 0;
ERROR_RETURN:
    sytbl_free(elm);
    return err;
} 

int sytbl_elmnt_cucontex(CUcontext *ctx, char *ident) {
    
    int err;

    sytbl_element_s *elm = sytbl_alloc();
    sytable_enforce_ident(ident,elm);
    elm->type = CUDA_CONTEX;
    elm->ctx = ctx;
    err = sytbl_insert(elm)
    RETURN_ERROR(err);

    return 0;
ERROR_RETURN:
    sytbl_free(elm);
    return err;
}



sytbl_element_s * sytbl_alloc(void) {
    sytbl_element_s *ste = (sytbl_element_s*)malloc(sizeof(sytbl_element_s));
    memset(ste,0,sizeof(sytbl_element_s));
    return ste;
}

void sytbl_element_free(sytbl_element_s *ste) {
    assert(ste != NULL);
    free(ste);
}

int sytbl_delete(const unsigned char *ident,sytbl_element_s **element) {
    return -1;
}

int sytbl_lookup(const unsigned  char ident[MAX_IDENT_LEN+1],sytbl_element_s **element) {
    assert(element != NULL);

    const uint64_t key = HASH(ident);
     
    fprintf(stderr,"key: %d\n",(int)key); 
    sytbl_element_s *elmnt = *SYTBL_GET_ELEMENT_PTR(key);

    
    while(elmnt && strncmp((const char *)elmnt->ident,(const char *)ident,MAX_IDENT_LEN)) { 
        fprintf(stderr,"lookpup2 \n");
        elmnt = elmnt->next;
    }
   
    *element = elmnt;
    
    return (elmnt) ? 0: -1;//TODO: more error
} 

int sytbl_insert(sytbl_element_s * element) {
    assert(element != NULL);
    assert(*(element->ident) != '\0'); 
    fprintf(stderr,"element->ident: %s\n",element->ident);       
 
    const uint64_t key = HASH(element->ident);
    fprintf(stderr,"insert hash key:%d\n",(int)key);
    sytbl_element_s **head_ptr = SYTBL_GET_ELEMENT_PTR(key);
    element->next = *head_ptr;
    *head_ptr = element;

    return 0;
}

/* 
    Helper function , enforces invariant strlen < MAX_IDENT_LEN+1 on input,
    use memcpy if this already is true, NULL terminate the string
*/
static void sytbl_enforce_ident(const char *str,sytbl_element_s *element) {
    assert(str != NULL); 
    assert(element != NULL);

    const size_t len = strlen(str);
    assert(len < MAX_IDENT_LEN +1); 
    
    memcpy(element->ident,str,len);
    memset(element->ident + len,'\0',MAX_IDENT_LEN +1 -len);

}

#ifdef LAUDEN
//hash algo from Lauden compiler book
static uint64_t __lauden(const unsigned char key[MAX_IDENT_LEN +1]) {
    #define LAU_SHIFT  4U
    uint64_t hash = 0UL;
    
    for(int i = 0 ;key[i] && i < MAX_IDENT_LEN ; i++) {
        assert(key[i]);
        hash = ((hash << LAU_SHIFT) + key[i]) %  SYMTBL_LEN;
    } 
    
    return hash;
    #undef LAU_SHIFT
}
#endif
#ifdef DJB2
/*
Std implementation of djb2 
Source: http://www.cse.yorku.ca/~oz/hash.html 
*/
static uint64_t __djb2(const unsigned char string[MAX_IDENT_LEN + 1]) {
    
    int c;
    uint64_t hash = 5381;
    const unsigned char *str = string;    

    for(int len = 0; (c = *str++) && len <MAX_IDENT_LEN; len++)  
        hash = ((hash<<5) + hash) + c; /* hash *33 + c*/
    assert(c == '\0');//is at eos

    return hash % SYMTBL_LEN;//good idea ?    
}
#endif



#ifdef TEST_SYMTBL

#include "unit_test.h"

sytbl_element_s *test_array = (sytbl_element_s []){
    {.ident = "Test1", .type = SYM_INTEGER, .num = 7},
    {.ident = "Test2", .type = SYM_INTEGER, .num = 11},
    {.ident = "a",.type = SYM_INTEGER, .num = 57}
};
static const int num_tests = 3; //MUST BE SET MANUALY, my bad. 

static const char *test_lookup_fail[] = {"fail1","2fail"};
static const int num_test_lof = sizeof(test_lookup_fail) / sizeof(*test_lookup_fail);

int main(int argc, char *argv[]) {


    START_TEST;
    sytbl_init();    

    PRINT_TEST("TEST INSERT AND LOOKUP SUCCES");
    
    for(int i = 0; i < num_tests; i++) {      
        const sytbl_element_s test_element = test_array[i];

        fprintf(stderr,"ident %s type: %d\n",test_element.ident,test_element.type);
        switch(test_element.type) {
            case SYM_INTEGER:
                ASSERT_TEST("sytbl_insert INT",sytbl_elmnt_int(test_element.num,(const char*)test_element.ident) == 0);
                break;
            default:
                ASSERT_TEST("default ",0);
                break;
        }
     }   

    for(int i = 0; i<num_tests; i++) {
        const sytbl_element_s test_element = test_array[i];
        sytbl_element_s *e = NULL;
        ASSERT_TEST("sytbl_lookup ",sytbl_lookup(test_element.ident,&e) == 0);
        if(e == NULL) continue;
        ASSERT_TEST("sytbl_lookup type",test_element.type == e->type);
        switch(e->type) {
            case SYM_INTEGER:
                ASSERT_TEST("Correct num",test_element.num == e->num);
                break;
            default:
                continue;
        }
    }

    PRINT_TEST("TEST LOOKUP FAIL");

    for(int i = 0; i < num_test_lof; i++) {
        sytbl_element_s *e =  NULL;
        ASSERT_TEST("Lookup fail on wrong ident",sytbl_lookup(test_lookup_fail[i],&e));
    }

    END_TEST;

    return 0;
}
#endif
