#ifndef SYMTABL_H
#define SYMTABL_H

/* Symbole table */



/*-- Defines --*/

#define MAX_IDENT_LEN    20U //TODO: add sym


/*-- Type declarations --*/

typedef enum {
    TEST_ELEMENT_SYTBL =-2,
    START_ELEMENT_TYPE =-1,
    CUDA_CONTEX = 0,
    CUDE_FUNCTION,
    SYM_INTEGER,
    DOUBLE,
    INTPTR,
    DOUBLEPTR,
    SYM_CUDEVICE,
    SYTBL_ELEMENT_TYPE_COUNT
}sytbl_element_type_e;

typedef struct sytbl_element_ {
    unsigned char ident[MAX_IDENT_LEN +1];
    sytbl_element_type_e type;
    union {
        int num;
        size_t size;
        void *element;
        CUdevice dev;
        CUcontext *ctx;
    };
    struct sytbl_element_ *next;
    struct sytbl_element_ *prev;//NIU
}sytbl_element_s;


/* --Function prototypes-- */

void sytbl_init(void);
void sytbl_destroy(void);

int sytbl_lookup(const unsigned char *ident,sytbl_element_s **ste_ptr);
int sytbl_insert(sytbl_element_s *ste);
int sytbl_delete(const unsigned char *ident,sytbl_element_s **ste_ptr);


/*
    Function for inserting elements into symtable
    This is slitly padentic.. 
*/
int sytbl_elmnt_int(const int num,const char *ident);
int sytbl_elmnt_cudevice(CUdevice dev,char *ident);
int sytbl_elmnt_cucontex(CUcontext *ctx, char *ident);

/* Functions for getting spesific types out of the symtabl */
int sytbl_lookup_int(int *num,char *ident);
int sytbl_lookup_cucontex(CUcontext *ctx, char *ident);

#endif
