#include <stdio.h>


extern "C" __global__ void test_kernel(void) {
    
    printf("Hello from cuda nvcc plebs!\n");

}



int main(void) {

    int devCount;

    cudaGetDeviceCount(&devCount);
    fprintf(stderr,"devCount %d\n",devCount);
   
    //devCount = 1; 

    for(int i = 0; i<devCount; i++) {
        cudaSetDevice(i);
        cudaFree(0);   
    }
 
    for(int i = 0; i<devCount; i++) {
         cudaSetDevice(i);
        test_kernel<<<1,1>>>();
    }

    for(int i = 0; i<devCount; i++) {
        cudaSetDevice(i);
        cudaDeviceReset();

    }
        
    
}
