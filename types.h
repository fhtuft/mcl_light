/* Types for mcl ligt */
#ifndef TYPES_H
#define TYPES_H
#include <stdint.h>

#include <cuda.h>


typedef enum {
    NOT_A_DEV_VAR = -1,  
    DEV_VAR_DOUBLE = 0,
    DEV_VAR_VARSIZE_DOUBLE,
    DEV_VAR_INT,
    DEV_VAR_VARSIZE_INT,
    DEV_VAR_COUNT
}dev_var_type_e;
typedef enum {
    NOT_A_DEV_VAR_GEOM = -1,
    DEV_VAR_GEOM_NONE = 0,
    DEV_VAR_GEOM_DIM1,   
    DEV_VAR_GEOM_DIM2,
    DEV_VAR_GEOM_DIM3,
    DEV_VAR_GEOM_COUNT
}dev_var_geom_e;
typedef struct device_vars_ {
    dev_var_type_e type;
    dev_var_geom_e geom_type;
    size_t sizeof_this; //must be same as total size of vars sizeof_vars
/*
    NONE: NULL
    DIM1: [UP,DOWN]
    DIM2: [NORTH,SOUTH,EAST,WEST]
    DIM3: [UP,DOWN,NORTH,SOUTH,EAST,WEST]
*/
    int **geom; /* witch device var is conected to who. And where, in respect to geom type */
/*
    NONE: NULL
    DIM1: x
    DIM2: x,y
    DIM3: x,y,z
*/    
    size_t **dim; /* Dimension of varibles, in bytes*/
    size_t var_count;
    size_t *sizeof_vars;
    CUdeviceptr **device_vars;
}dev_var_s;

typedef struct host_vars_ {
    cu_type_e type;
    size_t var_count;
    size_t *sizeof_vars;
    void **host_vars;
}host_vars_s;


typedef dev_var_com_type_ {
    NOT_A_DEV_VAR_COM_TYPE = -1,
    DEV_VAR_COM_TYPE_COUNT,
}dev_var_com_type_s;

typedef struct dev_var_com_ {
    dev_var_com_type_e type;
    size_t buf_count;
    size_t *sizeof_buf;
    void **host_buffers;
    CUdeviceptr **device_buffers;
    int **topo; /* Topology of the comunicator */
}dev_var_com_s;




#endif
