/* SIMPEL UNIT TEST 
USE:
START_TEST;
ASSERT_TEST(msg,test); msg: const char[], test: bool
END_TEST; Summarize the test, not necessary
*/
#ifndef UNIT_TEST_H
#define UNIT_TEST_H

#include <stdio.h> //fprintf,

#define INIT_TEST  
#define START_TEST static int TEST_COUNT =0;static int TEST_PASSED =0;fprintf(stderr,"UNIT TEST OF FILE:%s DATA:%s \n",__FILE__,__DATE__)
#define ASSERT_TEST(msg,test) do { if(!(test)) {fprintf(stderr," [FAILED]");} else{fprintf(stderr," [PASSED]");TEST_PASSED++;};fprintf(stderr," UNIT TEST ID:%d LINE:%d MSG:%s \n",TEST_COUNT,__LINE__,msg); TEST_COUNT++;}while(0)
#define END_TEST do {if(TEST_COUNT == TEST_PASSED) {fprintf(stderr," [PASSED]");} else{fprintf(stderr," [FAILED]");};fprintf(stderr," UNIT TEST OF %s PASSED: %d of %d tests\n",__FILE__,TEST_PASSED,TEST_COUNT);}while(0)
#define PRINT_TEST(msg) fprintf(stderr,"UNIT TEST MSG:%s \n",msg)

#endif
