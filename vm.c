/**
* @file vm.c
* @brief MCL-VM
* @author fhtuft
* @bug No Known bugs
*/
#include <stdio.h>
#include <assert.h>
#include <stdint.h>
#include <cuda.h>
#include <builtin_types.h>
#include <inttypes.h>

#include "vm.h"

 
_Static_assert(sizeof(void *) == sizeof(uint64_t),"Not a 64 bit system");



/* DEBUG CONFIG */
#ifdef DEBUG_VM
#define DEBUG_VM_ADD_INSTR    1
#define DEBUG_VM_RUN          1
#endif

/*-- Defines --*/

#define VM_INITIAL_PROG_POS      0



//#if DEBUG_VM_ADD_INSTR
static CUresult inline _driver_error(CUresult err,const char* ffname,const int line_nr) {
    if(err != CUDA_SUCCESS) {
        const char *err_string; 
        cuGetErrorString((err),&err_string); 
        fprintf(stderr,"MCL_CUDA_ERROR in %s %d: %s \n",ffname,line_nr,err_string); 
    }
    return err;
}

#define DRIVER_ERROR(ERR) if(_driver_error((ERR),__FUNCTION__,__LINE__) != CUDA_SUCCESS){goto ERROR_DRIVER;} 
//#else 
//#define DRIVER_ERROR(ERR)  (ERR)
//#endif



//DEBUG
static void print_vm_s(vm_s *ptr) {
        fprintf(stderr,"print_vm_s\n");
    for(int i = ptr->prog_pos; i<ptr->prog_len; i++) {
        fprintf(stderr,"pos: %d 0x%" PRIx64 "\n",i,ptr->prog[i]);
    }
}

#define NEXT(PRG)    (*(PRG)++) //DREFERANCE THE INSTRUCTION, ADVANCE PRG
#define DEREF(PRG) ((uint64_t *)(*(PRG))) //DEREF FROM PRG MEM


vm_s * alloc_vm(size_t len) {
    const size_t sizeof_prog = len*sizeof(uint64_t);
    vm_s *ptr = (vm_s *)malloc(sizeof(vm_s));
    memset(ptr,0,sizeof(vm_s));
    uint64_t *prog = (uint64_t*)malloc(sizeof_prog);
    memset(prog,0,sizeof_prog);
    ptr->prog = prog;
    ptr->prog_len = (uint64_t)len;
    ptr->prog_pos = VM_INITIAL_PROG_POS;
    
    return ptr;
}

void free_vm(vm_s *ptr) {
    assert(ptr != NULL);
    assert(ptr->prog != NULL);
    free(ptr->prog);
    free(ptr);
}




void vm_run(const vm_s *const ptr,const uint64_t start) {
    
    
    static const void * jmp_tbl[] = {
    &&SUBRUTINE_LAUNCH,
    &&SUBRUTINE_SEND,
    &&SUBRUTINE_RECV,
    &&SUBRUTINE_LOAD,
    &&SUBRUTINE_ULOAD,
    &&SUBRUTINE_POPCONTEX,
    &&SUBRUTINE_PUSHCONTEX,
    &&SUBRUTINE_MEMCHTD,
    &&SUBRUTINE_MEMCDTH,
    &&SUBRUTINE_MEMCHTDASYN,
    &&SUBRUTINE_MEMCDTHASYN,
    &&SUBRUTINE_SETSHARED,
    &&SUBRUTINE_MEMMOVE,
    &&SUBRUTINE_SYNC,
    &&SUBRUTINE_LAUNCH_PP,
    &&SUBRUTINE_MEMCDTH_PP,
    &&SUBRUTINE_MEMCHTD_PP,
    &&SUBRUTINE_MEMCDTHASYN_PP,
    &&SUBRUTINE_MEMCHTDASYN_PP,
    &&SUBRUTINE_SETSHARED_PP,
    &&SUBRUTINE_MEMMOVE_PP,
    &&SUBRUTINE_SYNC_PP,
    &&SUBRUTINE_HALT,
    };
    
    
    const uint64_t * prg = &(ptr->prog[start]);


/* VMs subrutines */

LOOP_ON_JMP_TBL:    
    goto *jmp_tbl[NEXT(prg)];
SUBRUTINE_LAUNCH:
    
    DRIVER_ERROR(cuLaunchKernel((CUfunction)(*DEREF(prg)), /* function */
    (unsigned)(*DEREF(prg + 1)),(unsigned)(*DEREF(prg + 2)),(unsigned)(*DEREF(prg + 3)), /* girdDim */
    (unsigned)(*DEREF(prg + 4)),(unsigned)(*DEREF(prg + 5)),(unsigned)(*DEREF(prg + 6)), /* blockDim */
    (unsigned)(*DEREF(prg + 7)), /* sharedMem */
    (CUstream)(*DEREF(prg + 8)), /* stream */
    (void **)(DEREF(prg + 9)), /* kernel param */
    NULL)); /* extra */
    
    prg += 11;//+1 for extra, 
    fprintf(stderr,"LAUNC_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_SEND:
    fprintf(stderr,"SEND_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_RECV:
    fprintf(stderr,"RECV_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_LOAD:
assert(0);
SUBRUTINE_ULOAD:
assert(0);
SUBRUTINE_PUSHCONTEX:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    prg += 1;
goto LOOP_ON_JMP_TBL;
SUBRUTINE_POPCONTEX:
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); 
    prg += 1;
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCDTH:
    DRIVER_ERROR(cuMemcpyDtoH((void *)*DEREF(prg),(CUdeviceptr)*DEREF(prg + 1),(size_t)*DEREF(prg + 2)));
    prg += 3;
    fprintf(stderr,"LOAD_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCHTD:
    DRIVER_ERROR(cuMemcpyHtoD((CUdeviceptr)*DEREF(prg),(void*)*DEREF(prg + 1),(size_t)*DEREF(prg + 2)));
    prg += 3;
    fprintf(stderr,"ULOAD_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCDTHASYN:
    DRIVER_ERROR(cuMemcpyDtoHAsync((void *)*DEREF(prg),(CUdeviceptr)*DEREF(prg + 1),(size_t)*DEREF(prg + 2),(CUstream)*DEREF(prg + 3)));
    prg += 4;
    fprintf(stderr,"LOAD_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCHTDASYN:
    DRIVER_ERROR(cuMemcpyHtoDAsync((CUdeviceptr)*DEREF(prg),(void *)*DEREF(prg + 1),(size_t)*DEREF(prg + 2),(CUstream)*DEREF(prg + 3)));
    prg += 4;
    fprintf(stderr,"ULOAD_SR\n");
SUBRUTINE_SETSHARED:
    DRIVER_ERROR(cuCtxSetSharedMemConfig((CUsharedconfig)*DEREF(prg)));
    prg += 1; 
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMMOVE:
assert(0);
SUBRUTINE_SYNC:
    DRIVER_ERROR(cuCtxSynchronize() ); 
goto LOOP_ON_JMP_TBL;
SUBRUTINE_LAUNCH_PP:
    //print_vm_s(ptr);
    //fprintf(stderr,"CONTEX (prg) 0x%x &ctx 0x%x \n",*DEREF(prg),prg);   
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    //fprintf(stderr,"DEREF 1:0x%x 2:0x%x 3:0x%x 4:0x%x 5:0x%x 6:0x%x 7:0x%x 8:0x%x 9:0x%x 10:0x%x 11:0x%x \n",*DEREF(prg + 1),*DEREF(prg + 2),*DEREF(prg + 3),*DEREF(prg + 4),*DEREF(prg + 5),*DEREF(prg + 6),*DEREF(prg + 7),*DEREF(prg + 8),*DEREF(prg + 9),DEREF(prg + 10),DEREF(prg + 11));
  
    #if 1
    DRIVER_ERROR(cuLaunchKernel((CUfunction)(*DEREF(prg + 1)), /* function */
    (unsigned)(*DEREF(prg + 2)),(unsigned)(*DEREF(prg + 3)),(unsigned)(*DEREF(prg + 4)), /* girdDim */
    (unsigned)(*DEREF(prg + 5)),(unsigned)(*DEREF(prg + 6)),(unsigned)(*DEREF(prg + 7)), /* blockDim */
    (unsigned)(*DEREF(prg + 8)), /* sharedMem */
    (CUstream)(*DEREF(prg + 9)), /* stream */
    (void **)(DEREF(prg + 10)), /* kernel param */
    NULL)); /* extra */
    #endif
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); //TODO: is this right! 
    prg += 12;
    fprintf(stderr,"LAUNC_SR\n");
goto LOOP_ON_JMP_TBL;

SUBRUTINE_MEMCDTH_PP:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    DRIVER_ERROR(cuMemcpyDtoH((void *)*DEREF(prg +1),(CUdeviceptr)*DEREF(prg + 2),(size_t)*DEREF(prg + 3)));
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); 
    prg += 4;
    fprintf(stderr,"LOAD_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCHTD_PP:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    DRIVER_ERROR(cuMemcpyHtoD((CUdeviceptr)*DEREF(prg + 1),(void *)*DEREF(prg + 2),(size_t)*DEREF(prg + 3)));
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); 
    prg += 4;
    fprintf(stderr,"ULOAD_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCDTHASYN_PP:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    DRIVER_ERROR(cuMemcpyDtoHAsync((void *)*DEREF(prg + 1),(CUdeviceptr)*DEREF(prg + 2),(size_t)*DEREF(prg + 3),(CUstream)*DEREF(prg + 4)));
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); 
    prg += 5;
    fprintf(stderr,"LOAD_SR\n");
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMCHTDASYN_PP:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    DRIVER_ERROR(cuMemcpyHtoDAsync((CUdeviceptr)*DEREF(prg + 1),(void *)*DEREF(prg + 2),(size_t)*DEREF(prg + 3),(CUstream)*DEREF(prg + 4)));
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); 
    prg += 5;
    fprintf(stderr,"ULOAD_SR\n");
SUBRUTINE_SETSHARED_PP:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    DRIVER_ERROR(cuCtxSetSharedMemConfig((CUsharedconfig)*DEREF(prg +1)));
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg)));
    prg += 2; 
goto LOOP_ON_JMP_TBL;
SUBRUTINE_MEMMOVE_PP:
;
SUBRUTINE_SYNC_PP:
    DRIVER_ERROR(cuCtxPushCurrent((CUcontext)(*DEREF(prg))));
    DRIVER_ERROR(cuCtxSynchronize()); 
    DRIVER_ERROR(cuCtxPopCurrent((CUcontext *)DEREF(prg))); 
    prg += 1;
SUBRUTINE_HALT:
   fprintf(stderr,"HALT_VM\n");     
return;

ERROR_DRIVER:
return;


}

#ifdef TEST_VM

#include "unit_test.h"
#define TEST_MEM1_COUNT      14
#define TEST_MEM1_DEVICE      0
#define TEST_MEM1_PARAM_COUNT 3
const char moduleName[] = "_test_kernel.ptx";
const char kernelName[] = "test_kernel";
static uint64_t test_mem1[TEST_MEM1_COUNT]; 

int main(void) {

    /* Init of test */
    DRIVER_ERROR(cuInit(0));
    
    int deviceCount;
    DRIVER_ERROR(cuDeviceGetCount(&deviceCount)); 
    
    CUdevice dev;
    DRIVER_ERROR(cuDeviceGet(&dev,TEST_MEM1_DEVICE));
    
    CUcontext ctx;
    DRIVER_ERROR(cuCtxCreate(&ctx, 0, dev));
    
    CUdeviceptr c;
    DRIVER_ERROR(cuMemAlloc(&c,sizeof(int)));
    
    CUmodule module;
	DRIVER_ERROR(cuModuleLoad(&module,moduleName));
	
    CUfunction function;
	DRIVER_ERROR(cuModuleGetFunction(&function, module, kernelName));
    
 
    DRIVER_ERROR(cuCtxPopCurrent(&ctx));  
    
    void *test_mem1_param[TEST_MEM1_PARAM_COUNT];
    int a = 1;
    int b = 2;

    unsigned cf_p[9] =  {1,1,1,1,1,1,0,0};

    test_mem1_param[0] = &a; test_mem1_param[1] = &b;
    test_mem1_param[2] = &c;
    DRIVER_ERROR(cuCtxPushCurrent(ctx));  
    DRIVER_ERROR(cuLaunchKernel(function,1,1,1,1,1,1,0,0,test_mem1_param,NULL));
    int host_c;  
    DRIVER_ERROR(cuMemcpyDtoH(&host_c,c,sizeof(int)));
    fprintf(stderr,"Host C %d\n",host_c);    
    DRIVER_ERROR(cuCtxPopCurrent(&ctx));

    test_mem1[0] = LAUNCH_PP;
    fprintf(stderr,"CONTEX 0x%x &ctx 0x%x  0x%x\n",ctx,&ctx,(uint64_t)ctx);   
    
 
    test_mem1[1] = (uint64_t)&ctx;
    //fprintf(stderr,"test_mem[1] 0x%x\n",*(test_mem1[1]));
    test_mem1[2] = (uint64_t)&function;
    test_mem1[3] = (uint64_t)&cf_p[0]; test_mem1[4] = (uint64_t)&cf_p[1]; test_mem1[5] = (uint64_t)&cf_p[2];
    test_mem1[6] = (uint64_t)&cf_p[3]; test_mem1[7] = (uint64_t)&cf_p[4]; test_mem1[8] = (uint64_t)&cf_p[5];
    test_mem1[9] = (uint64_t)&cf_p[6];
    test_mem1[10] = (uint64_t)&cf_p[7];
    test_mem1[11] = (uint64_t)test_mem1_param;
    test_mem1[12] = (uint64_t)NULL;
     
    test_mem1[13] = HALT;

    fprintf(stderr,"ctx:0x%x function:0x%x test_mem1_param:0x%x \n",ctx,function,test_mem1_param);
    vm_s test_vms1;
    test_vms1.prog = test_mem1; 
    test_vms1.prog_len = 14;
    test_vms1.prog_pos = 0;

    START_TEST;
    
    
#if 1
    //print_vm_s(&test_vms1);
    vm_run(&test_vms1,0);
#endif

    END_TEST;

    return 0;
   ERROR_DRIVER:
    return 1; 
}

#endif 
