/* header for mcl vm*/
#ifndef VM_H
#define VM_H


/*-- Defines --*/

/*
ISA MCL-VM:
This is the current instructrions supported by the vm
*/ 
#define LAUNCH        0UL
#define SEND          1UL
#define RECV          2UL
#define LOAD          3UL
#define ULOAD         4UL
#define PUSHCON       5UL
#define POPCON        6UL
#define MEMCDTH       7UL
#define MEMCHTD       8UL
#define MEMCDTDA      9UL
#define MEMCHTDA     10UL
#define SETSHARD     11UL
#define MEMMOVE      12UL
#define SYNC         13UL
#define LAUNCH_PP    14UL
#define MEMCDTH_PP   15UL
#define MEMCHTD_PP   16UL
#define MEMCDTHA_PP  17UL
#define MEMCHTDA_PP  18UL
#define SETSHARD_PP  19UL
#define MEMMOVE_PP   20UL
#define SYNC_PP      21UL
#define HALT         22UL 





typedef struct vm_ {
    uint64_t *prog;    
    uint64_t  prog_len; /* Max length of prog */
    uint64_t  prog_pos; /* Current pos in prog */
}vm_s;




/*-- Function prototypes --*/
vm_s * alloc_vm(size_t len); 
void free_vm(vm_s *ptr); 
void vm_run(const vm_s *const ptr,const uint64_t start); 



/* Helper function */
static inline void vm_add_instr(const uint64_t instr,vm_s *vm) {
    assert(vm->prog_pos > vm->prog_len);
    (vm->prog)[vm->prog_pos++] = instr;
}

static inline void vm_finalize(vm_s *vm,const uint64_t prog_pos) {
    vm->prog_pos = prog_pos; 
}



#endif 
